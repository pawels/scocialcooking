//
//  RequestsViewController.m
//  cookingApp
//
//  Created by Pawel Sobocinski on 07.01.2014.
//  Copyright (c) 2014 Pawel Sobocinski. All rights reserved.
//

#import "RequestsViewController.h"
#import "SCApiClient.h"
#import "RequestCell.h"
#import "Application.h"
#import "RequestViewController.h"
#import "SCHelp.h"
#import <UIImageView+AFNetworking.h>

@interface RequestsViewController () <CLLocationManagerDelegate>
    @property (nonatomic, strong) NSArray *results;
    @property (nonatomic, strong) CLLocationManager *locationManager;
    @property (nonatomic, strong) CLLocation *currentLocation;
@end

@implementation RequestsViewController


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tabBarController.tabBar.hidden = NO;
    _results = [[NSMutableArray alloc] init];
    

    [self updateLocation];
    [self featchApplications:@{@"page": @"0"}];

}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self featchApplications: @{@"page": @"0"}];
}

- (void)updateLocation {
    [self.locationManager startUpdatingLocation];
}

#pragma mark - CLLocationManagerDelegate methods

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    _currentLocation = [locations lastObject];
    //NSLog(@"Locations: %@",locations);
    //NSLog(@"Location: %@",_currentLocation);
     //NSLog(@"AAAAAAA: lat%f - lon%f", _currentLocation.coordinate.latitude, _currentLocation.coordinate.longitude);
    
    [self.locationManager stopUpdatingLocation];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    self.locationManager = nil;
}

- (CLLocationManager *)locationManager {
    if (!_locationManager) {
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
        _locationManager.delegate = self;
    }
    return _locationManager;
}


#pragma mark - Requests operations


-(void) featchApplications: (NSDictionary *) params {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    NSURLSessionDataTask *task = [[SCApiClient sharedClient] applications:params completion:^(NSArray *results, NSError *error) {
                                                                     if (results) {
                                                                         [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                                                         _results = results;
                                                                         [self.tableView reloadData];
                                                                         [self.refreshControl endRefreshing];
                                                                     } else {
                                                                         NSLog(@"ERROR: %@", error);
                                                                     }
                                                                 }];
    
    
}

-(void) addApplication {
//    UIStoryboardSegue * addSuege = [[UIStoryboardSegue alloc] initWithIdentifier:@"addApplication" source:self destination:<#(UIViewController *)#>]
//    [self prepareForSegue:(UIStoryboardSegue *) sender:<#(id)#>]

    NSLog(@"presed");
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.results.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 110.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"RequestCell";
    RequestCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    Application *application =  self.results[indexPath.row];
    cell.applicationId = application.applicationId;
    //NSLog(@"App: %@", application);
    UIImage *applicationImagePlaceholder = [UIImage imageNamed:@"default_avatar.png"];
    [cell.applicationImage setImageWithURL: [SCHelp fbAvatarUrl: application.user.fbUserId] placeholderImage:applicationImagePlaceholder];
    cell.applicationDistance.text = [SCHelp textDistance: [SCHelp distanceFrom: _currentLocation and: application.loc]];
    cell.applicationUser.text = application.user.username;
    cell.applicationCreated.text = [SCHelp dateForamted: application.created];
    cell.applicationCity.text = application.locationName;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView  willDisplayCell:(RequestCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *postname = [NSString stringWithFormat:@"labelUpdate_%@", cell.applicationId];
    [[NSNotificationCenter defaultCenter] addObserver: cell selector:@selector(updateLabel:) name:postname object: nil];
}

- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(RequestCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *postname = [NSString stringWithFormat:@"labelUpdate_%@", cell.applicationId];\
    [[NSNotificationCenter defaultCenter] removeObserver:cell name:postname object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    for (RequestCell *cell in self.tableView.visibleCells) {
        NSString *postname = [NSString stringWithFormat:@"labelUpdate_%@", cell.applicationId];
        [[NSNotificationCenter defaultCenter] removeObserver:cell name:postname object:nil];
    }
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if([segue.identifier isEqualToString:@"showApplication"]) {
        RequestViewController *destViewController = segue.destinationViewController;
        NSIndexPath *ip = [self.tableView indexPathForSelectedRow];
        
        
        destViewController.application = [_results objectAtIndex:ip.row];
        destViewController.currentLocation = _currentLocation;
    }
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}



- (IBAction)refreshData:(id)sender {
    [self featchApplications:@{@"page": @"0"}];
}
@end
