//
//  Product.m
//  sCookingRestKit_v2
//
//  Created by Pawel Sobocinski on 30.12.2013.
//  Copyright (c) 2013 Pawel Sobocinski. All rights reserved.
//

#import "Product.h"
#import "SCApiClient.h"


@implementation Product

-(void)setValue:(id)value forKey:(NSString *)key
{
    if ([key isEqualToString:@"_id"]) {
        self.productId = value;
    }
    else if ([key isEqualToString:@"user"]) {
        self.user = [User objectWithProperties: value];
    }
    else if ([key isEqualToString:@"name"]) {
        self.name = value;
    }
    else if ([key isEqualToString:@"created"]) {
        self.created = [self.dateFormater dateFromString:value];
    }
}


-(void) saveProduct:( void (^)(Product *product, NSError *error) )completion {
    NSDictionary *params = @{
                             @"user": self.user.userId,
                             @"name": self.name
                             };
    
    NSLog(@"params %@,", params);
    [[SCApiClient sharedClient] saveProduct:params completion: completion];
}

-(void) removeProduct:( void (^)(Product *product, NSError *error) )completion {
    NSDictionary *params = @{
                             @"user": self.user.userId,
                             @"name": self.name
                             };
    
    NSLog(@"params %@,", params);
    [[SCApiClient sharedClient] saveProduct:params completion: completion];
}

@end
