//
//  RecipeViewController.h
//  socialCooking
//
//  Created by Pawel Sobocinski on 12.01.2014.
//  Copyright (c) 2014 Pawel Sobocinski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Recipe.h"

@interface RecipeViewController : UITableViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>


//outlets
@property (strong, nonatomic) IBOutlet UITextField *recipeNameText;
@property (strong, nonatomic) IBOutlet UIImageView *recipePhoto;
- (IBAction)addPhoto:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *productsView;
- (IBAction)saveRecipe:(id)sender;
@end
