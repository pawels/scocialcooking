//
//  MainTabViewController.h
//  socialCooking
//
//  Created by Pawel Sobocinski on 14.01.2014.
//  Copyright (c) 2014 Pawel Sobocinski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainTabViewController : UITabBarController

@property (strong, nonatomic) IBOutlet UITabBar *tabBarOutlet;

@end
