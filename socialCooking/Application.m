//
//  Application.m
//  socialCooking
//
//  Created by Pawel Sobocinski on 08.01.2014.
//  Copyright (c) 2014 Pawel Sobocinski. All rights reserved.
//

#import "Application.h"
#import "SCHelp.h"

@implementation Application

static void * kBackgroundColorObservationContext = &kBackgroundColorObservationContext;

-(void)setValue:(id)value forKey:(NSString *)key
{
    if ([key isEqualToString:@"_id"]) {
        self.applicationId = value;
    }
    else if ([key isEqualToString:@"user"]) {
        self.user = [User objectWithProperties: value];
    }
    else if ([key isEqualToString:@"products"]) {
        if(value) {
            id frst_object = [value firstObject];
            
            if([frst_object isKindOfClass:[NSDictionary class]]) {
                NSMutableArray *products = [[NSMutableArray alloc] init];
                for (NSDictionary *product in value) {
                    NSMutableDictionary *pp = [NSMutableDictionary dictionaryWithDictionary:product];
                    [pp removeObjectForKey:@"user"];
                    [products addObject: [Product objectWithProperties:pp]];
                }
                self.products = products;
            }
            else {
                self.products = value;
            }
        }
        
    }
    else if ([key isEqualToString:@"loc"]) {
        self.loc = CLLocationCoordinate2DMake([[[value objectForKey:@"coordinates"] objectAtIndex:1] doubleValue], [[[value objectForKey:@"coordinates"] objectAtIndex:0] doubleValue]);
    }
    else if ([key isEqualToString:@"created"]) {
        self.created = [self.dateFormater dateFromString:value];
    }
}

-(NSString *) locationName {
    if (!_locationName) {
        [self addObserver: self
                    forKeyPath:@"locationName"
                       options:NSKeyValueObservingOptionNew
                  context:kBackgroundColorObservationContext];
        
        
        _locationName = [NSString stringWithFormat: @""];
        [SCHelp getCityNameFrom: [[CLLocation alloc] initWithLatitude: _loc.latitude longitude:_loc.longitude] completion:^(NSString *locationName, NSError *error) {
            if(error) {
                NSLog(@"APPLoc Error: %@", error);
            }
            else {
                //NSLog(@"APPLoc: %@", locationName);
                NSString *postname = [NSString stringWithFormat:@"labelUpdate_%@", _applicationId];
                [[NSNotificationCenter defaultCenter] postNotificationName: postname object: locationName];
                _locationName = locationName;
            }
        }];
    }
    
    return _locationName;
}

-(void) saveApplication:( void (^)(Application *application, NSError *error) )completion {
    NSDictionary *params = @{
                             @"user": self.user.userId,
                             @"products": self.products,
                             @"loc": @{
                                     @"type": @"Point",
                                     @"coordinates": @[[NSString stringWithFormat:@"%lf", self.loc.longitude], [NSString stringWithFormat:@"%lf", self.loc.latitude]]
                                     }
                            };

    
    
    
    NSLog(@"params %@,", params);
    [[SCApiClient sharedClient] saveApplication: params completion:completion];
}


- (void)dealloc
{
    //[self o]
    //[self removeObserver:self forKeyPath:@"locationName" context:kBackgroundColorObservationContext];
    //[super dealloc];
}


@end
