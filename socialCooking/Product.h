//
//  Product.h
//  sCookingRestKit_v2
//
//  Created by Pawel Sobocinski on 30.12.2013.
//  Copyright (c) 2013 Pawel Sobocinski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SCModel.h"
#import "User.h"

@interface Product : SCModel

@property (nonatomic, strong)  NSString *productId;
@property (nonatomic, strong)  NSString *name;
@property (nonatomic, strong)  NSDate *created;
@property (nonatomic, strong)  NSDate *updated;
@property (nonatomic, strong) User *user;

-(void) saveProduct:( void (^)(Product *product, NSError *error) )completion ;

@end
