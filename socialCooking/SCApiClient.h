//
//  SCApiClient.h
//  cookingApp
//
//  Created by Pawel Sobocinski on 07.01.2014.
//  Copyright (c) 2014 Pawel Sobocinski. All rights reserved.
//

#import "AFHTTPSessionManager.h"
#import "Product.h"
#import "Recipe.h"
#import "User.h"
#import "Application.h"
#import <Accounts/Accounts.h>
#import <Social/Social.h>

@interface SCApiClient : AFHTTPSessionManager

@property (nonatomic, strong) ACAccountStore *accountStore;

+ (SCApiClient *)sharedClient;
- (NSURLSessionDataTask *)applications: (NSDictionary *) params completion:( void (^)(NSArray *results, NSError *error) )completion;
- (NSURLSessionDataTask *)appgeo: (NSDictionary *) params completion:( void (^)(NSArray *results, NSError *error) )completion;
- (NSURLSessionDataTask *)saveApplication :(NSDictionary *) params completion:( void (^)(Application *application, NSError *error) )completion;
- (NSURLSessionDataTask *)products :(NSDictionary *) params completion:( void (^)(NSArray *results, NSError *error) )completion;

//product
- (NSURLSessionDataTask *)saveProduct :(NSDictionary *) params completion:( void (^)(Product *product, NSError *error) )completion ;
- (NSURLSessionDataTask *)removeProduct :(NSString *) productId completion:( void (^)(BOOL *success, NSError *error) )completion;

//recipe
- (NSURLSessionDataTask *)saveRecipe :(NSDictionary *) params completion:( void (^)(Recipe *recipe, NSError *error) )completion ;
- (NSArray *) recipesForResponse: (NSDictionary *) response;
- (NSURLSessionDataTask *)recipes :(NSDictionary *) params completion:( void (^)(NSArray *results, NSError *error) )completion;
- (NSURLSessionDataTask *)recipesSearch :(NSDictionary *) params completion:( void (^)(NSArray *results, NSError *error) )completion;


//user
- (NSURLSessionDataTask *)userByEmail:(NSString *) email completion:( void (^)(BOOL found, User *user, NSError *error) )completion;
- (NSURLSessionDataTask *)saveFacebookUser:(NSDictionary *) fbParams completion:( void (^)(User *user, NSError *error) )completion;



@end
