    //
    //  SCApiClient.m
    //  cookingApp
    //
    //  Created by Pawel Sobocinski on 07.01.2014.
    //  Copyright (c) 2014 Pawel Sobocinski. All rights reserved.
    //

    #import "SCApiClient.h"
    #import "Application.h"
    #import <SVProgressHUD.h>
    #import "SCHelp.h"

    //#define API_BASE_URL @"http://localhost:3000/"
    #define API_BASE_URL @"http://social-cooking.herokuapp.com/"

    #define SERVICE_USERNAME @"socialcooking"
    #define SERVICE_PASSWORD @"sdaf3@#$TGDFG4534q"

    @implementation SCApiClient

    + (SCApiClient *)sharedClient {
        static SCApiClient *_sharedClient = nil;
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            NSURL *baseURL = [NSURL URLWithString:API_BASE_URL];
            
            NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
            [config setHTTPAdditionalHeaders:@{
                                               @"User-Agent" : @"SCApiClient iOS 1.0",
                                               @"Accept" : @"application/json",
                                               @"Content-Type" : @"application/json"
                                               }];
            
            NSURLCache *cache = [[NSURLCache alloc] initWithMemoryCapacity: 0//10 * 1024 * 1024
                                                              diskCapacity: 0//50 * 1024 * 1024
                                                                  diskPath:nil];
        
            [config setURLCache:cache];

            _sharedClient = [[SCApiClient alloc] initWithBaseURL:baseURL
                                             sessionConfiguration:config];
            [_sharedClient.requestSerializer setAuthorizationHeaderFieldWithUsername:SERVICE_USERNAME password:SERVICE_PASSWORD];
            _sharedClient.responseSerializer = [AFJSONResponseSerializer serializer];
        });
        
        return _sharedClient;
    }

    #pragma mark - applications

    -(NSArray *) applicationsForResponse: (NSDictionary *) response {
        NSMutableArray *applications = [[NSMutableArray alloc] init];
        for (NSDictionary *app in response) {
            [applications addObject: [Application objectWithProperties: app]];
        }
        return applications;
    }



    - (NSURLSessionDataTask *)applications:(NSDictionary *) params completion:( void (^)(NSArray *results, NSError *error) )completion {
        [SVProgressHUD show];
        
        NSString *applicationId = [params objectForKey:@"_id"];
        NSString *pathUrl;
        if([applicationId isEqualToString:@""] || !applicationId) {
            pathUrl = @"/applications";
        }
        else {
            pathUrl = [NSString stringWithFormat:@"/applications/%@", applicationId];
        }

        NSURLSessionDataTask *task = [self GET:pathUrl
                                    parameters:params
                                      
                                       success:^(NSURLSessionDataTask *task, id responseObject) {
                                           NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
                                           if (httpResponse.statusCode == 200) {
                                               NSArray *applications;
                                               if([applicationId isEqualToString:@""] || !applicationId) {
                                                   applications = [self applicationsForResponse:responseObject];
                                               }
                                               else {
                                                    NSLog(@"RESponse application ID: %@", responseObject);
                                                   Application *app = [[Application alloc] initWithProperties:responseObject];
                                                   applications = [NSArray arrayWithObject: app];
                                               }
                                               
                                            
                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                   [SVProgressHUD dismiss];
                                                   completion(applications, nil);
                                               });
                                           } else {
                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                   [SVProgressHUD showErrorWithStatus:@":("];
                                                   completion(nil, nil);
                                               });
                                               NSLog(@"Received: %@", responseObject);
                                               NSLog(@"Received HTTP %d", (int)httpResponse.statusCode);
                                           }
                                           
                                       } failure:^(NSURLSessionDataTask *task, NSError *error) {
                                           [SVProgressHUD showErrorWithStatus:@":("];
                                           NSLog(@"error: %@", error);
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               completion(nil, error);
                                           });
                                       }];
        return task;
    }


- (NSURLSessionDataTask *)appgeo:(NSDictionary *) params completion:( void (^)(NSArray *results, NSError *error) )completion {
    [SVProgressHUD show];
    
    
    NSURLSessionDataTask *task = [self GET:@"/applications/geo/"
                                parameters:params
                                  
                                   success:^(NSURLSessionDataTask *task, id responseObject) {
                                       NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
                                       if (httpResponse.statusCode == 200) {
                                           NSArray *applications = [self applicationsForResponse:responseObject];
                                            dispatch_async(dispatch_get_main_queue(), ^{
                                               [SVProgressHUD dismiss];
                                               completion(applications, nil);
                                           });
                                       } else {
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               [SVProgressHUD showErrorWithStatus:@":("];
                                               completion(nil, nil);
                                           });
                                           NSLog(@"Received: %@", responseObject);
                                           NSLog(@"Received HTTP %d", (int)httpResponse.statusCode);
                                       }
                                       
                                   } failure:^(NSURLSessionDataTask *task, NSError *error) {
                                       [SVProgressHUD showErrorWithStatus:@":("];
                                       NSLog(@"error: %@", error);
                                       //dispatch_async(dispatch_get_main_queue(), ^{
                                       //    completion(nil, error);
                                       // });
                                   }];
    return task;
}





- (NSURLSessionDataTask *)saveApplication :(NSDictionary *) params completion:( void (^)(Application *application, NSError *error) )completion {
    [SVProgressHUD show];
    NSString *applicationId = [params objectForKey:@"_id"];
    NSString *pathUrl;
    if([applicationId isEqualToString:@""] || !applicationId) {
        pathUrl = @"/applications";
    }
    else {
        pathUrl = [NSString stringWithFormat:@"/applications/%@", applicationId];
    }
    NSLog(@"pamars: %@", params);
    NSLog(@"path: %@", pathUrl);
    
    NSURLSessionDataTask *task = [self POST:pathUrl
                                 parameters:params
                   success:^(NSURLSessionDataTask *task, id responseObject) {
                      NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
                      if (httpResponse.statusCode == 200) {
                          NSLog(@"Repsonse: %@", responseObject);
                          Application *application = [[Application alloc] initWithProperties:responseObject];
                          
                          dispatch_async(dispatch_get_main_queue(), ^{
                              [SVProgressHUD dismiss];
                              completion(application, nil);
                          });
                      } else {
                          dispatch_async(dispatch_get_main_queue(), ^{
                              [SVProgressHUD showErrorWithStatus:@":("];
                              completion(nil, nil);
                          });
                          NSLog(@"Received: %@", responseObject);
                          NSLog(@"Received HTTP %d", (int)httpResponse.statusCode);
                      }
                      
                  } failure:^(NSURLSessionDataTask *task, NSError *error) {
                      [SVProgressHUD showErrorWithStatus:@":("];
                      NSLog(@"error: %@", error);
                  }];
    
    
    return task;
}



    #pragma mark - products

    -(NSArray *) productsForResponse: (NSDictionary *) response {
        NSMutableArray *products = [[NSMutableArray alloc] init];
        for (NSDictionary *prod in response) {
            [products addObject: [Product objectWithProperties: prod]];
        }
        return products;
    }


    - (NSURLSessionDataTask *)products :(NSDictionary *) params completion:( void (^)(NSArray *results, NSError *error) )completion {
        [SVProgressHUD show];
        NSURLSessionDataTask *task = [self GET:@"/products"
                                    parameters: params
                                       success:^(NSURLSessionDataTask *task, id responseObject) {
                                           NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
                                           if (httpResponse.statusCode == 200) {
                                               //NSLog(@"Repsonse: %@", responseObject);
                                               NSArray *products = [self productsForResponse:responseObject];
                                               
                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                   [SVProgressHUD dismiss];
                                                   completion(products, nil);
                                               });
                                           } else {
                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                   [SVProgressHUD showErrorWithStatus:@":("];
                                                   completion(nil, nil);
                                               });
                                               NSLog(@"Received: %@", responseObject);
                                               NSLog(@"Received HTTP %d", (int)httpResponse.statusCode);
                                           }
                                           
                                       } failure:^(NSURLSessionDataTask *task, NSError *error) {
                                           [SVProgressHUD showErrorWithStatus:@":("];
                                           NSLog(@"error: %@", error);
                                           //dispatch_async(dispatch_get_main_queue(), ^{
                                           //    completion(nil, error);
                                           // });
                                       }];
        
        return task;
    }


    - (NSURLSessionDataTask *)saveProduct :(NSDictionary *) params completion:( void (^)(Product *product, NSError *error) )completion {
        [SVProgressHUD show];
        NSString *productId = [params objectForKey:@"_id"];
        NSString *pathUrl;
        if([productId isEqualToString:@""] || !productId) {
            pathUrl = @"/products";
        }
        else {
            pathUrl = [NSString stringWithFormat:@"/products/%@", productId];
        }
        NSLog(@"pamars: %@", params);
        NSLog(@"path: %@", pathUrl);
        
        
         NSURLSessionDataTask *task = [self POST: pathUrl
                                      parameters:params
                                         success:^(NSURLSessionDataTask *task, id responseObject) {
                                             NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
                                             if (httpResponse.statusCode == 200) {
                                                 NSLog(@"Repsonse: %@", responseObject);
                                                 Product *product = [[Product alloc ] initWithProperties: responseObject];
                                                 
                                                 dispatch_async(dispatch_get_main_queue(), ^{
                                                     [SVProgressHUD dismiss];
                                                     completion(product, nil);
                                                 });
                                             } else {
                                                 dispatch_async(dispatch_get_main_queue(), ^{
                                                     [SVProgressHUD showErrorWithStatus:@":("];
                                                     completion(nil, nil);
                                                 });
                                                 NSLog(@"Received: %@", responseObject);
                                                 NSLog(@"Received HTTP %d", (int)httpResponse.statusCode);
                                             }
                                         } failure:^(NSURLSessionDataTask *task, NSError *error) {
                                             [SVProgressHUD showErrorWithStatus:@":("];
                                             NSLog(@"error: %@", error);
                                         }];
        
        

        
        return task;
    }

    - (NSURLSessionDataTask *)removeProduct :(NSString *) productId completion:( void (^)(BOOL *success, NSError *error) )completion {
        [SVProgressHUD show];
        
         NSString *pathUrl = [NSString stringWithFormat:@"/products/%@", productId];
        
        
        NSURLSessionDataTask *task = [self DELETE: pathUrl
                                       parameters:@{}
                                        success:^(NSURLSessionDataTask *task, id responseObject) {
                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
                                            if (httpResponse.statusCode == 200) {
                                                NSLog(@"Repsonse: %@", responseObject);
                                                
                                                
                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                    [SVProgressHUD dismiss];
                                                    completion(YES, nil);
                                                });
                                            } else {
                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                    NSString *errorMsg = [responseObject objectForKey:@"msg"];
                                                    [SVProgressHUD showErrorWithStatus: errorMsg];
                                                    
                                                    completion(NO, [responseObject objectForKey:[SCHelp errorFromString:errorMsg]]);
                                                });
                                                NSLog(@"Received: %@", responseObject);
                                                NSLog(@"Received HTTP %d", (int)httpResponse.statusCode);
                                            }
                                        } failure:^(NSURLSessionDataTask *task, NSError *error) {
                                            [SVProgressHUD showErrorWithStatus:@":("];
                                            NSLog(@"error: %@", error);
                                            completion(NO, error
    );
                                        }];
        
        
        
        
        return task;
    }


    #pragma mark - user
    - (NSURLSessionDataTask *)userByEmail :(NSString *) email completion:( void (^)(BOOL found, User *user, NSError *error) )completion {
        [SVProgressHUD show];
        NSString *path = [NSString stringWithFormat:@"/users/email/%@", email];
        NSURLSessionDataTask *task = [self GET: path
                                    parameters: @{}
                                       success:^(NSURLSessionDataTask *task, id responseObject) {
                                           NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
                                           if (httpResponse.statusCode == 200 || httpResponse.statusCode == 201) {
                                               //NSLog(@"Repsonse: %@", responseObject);
                                               
                                               //nie ma uzytkonika
                                               if ([[responseObject objectForKey:@"status"] isEqualToString:@"error"]) {
                                                   completion(NO, nil, nil);
                                               }
                                               else {
                                                   User *user = [[User alloc] initWithProperties:responseObject];
                                                   
                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                       //[SVProgressHUD dismiss];
                                                       completion(YES, user, nil);
                                                   });
                                               }
                                            
                                               
                                           }
                                           
                                           else {
                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                   completion(NO, nil, [SCHelp errorFromString: [responseObject description]]);
                                               });
                                               NSLog(@"Received: %@", responseObject);
                                               NSLog(@"Received HTTP %d", (int)httpResponse.statusCode);
                                           }
                                           
                                       }
                                       failure:^(NSURLSessionDataTask *task, NSError *error) {
                                           NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
                                           [SVProgressHUD showErrorWithStatus: error.localizedDescription];
                                           NSLog(@"serach error: %@", error);
                                           NSLog(@"serach error: resp: %@", httpResponse);
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               completion(NO, nil, error);
                                           });
                                       }];
        
        return task;
    }


- (NSURLSessionDataTask *)saveFacebookUser :(NSDictionary *) fbParams completion:( void (^)(User *user, NSError *error) )completion {
        [SVProgressHUD show];
    
    
    
        NSDictionary *params = @{
                                 @"password": [SCHelp generateRandomString: 8],
                                 @"email": [fbParams objectForKey:@"email"],
                                 @"facebook": fbParams,
                                 @"username": [NSString stringWithFormat:@"%@ %@", [fbParams objectForKey:@"first_name"], [[[fbParams objectForKey:@"last_name"] substringToIndex: 1] uppercaseString]],
                                 @"name":[fbParams objectForKey:@"name"]
                                };
    
    NSLog(@"params %@", params);
    
    
        NSURLSessionDataTask *task = [self POST:@"/users"
                                     parameters:params
                                        success:^(NSURLSessionDataTask *task, id responseObject) {
                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
                                          if (httpResponse.statusCode == 200) {
                                              //NSLog(@"Repsonse: %@", responseObject);
                                              
                                              User *user = [[User alloc] initWithProperties:responseObject];
                                              
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  [SVProgressHUD dismiss];
                                                  completion(user, nil);
                                              });
                                          } else {
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  [SVProgressHUD showErrorWithStatus:@":("];
                                                  completion(nil, [SCHelp errorFromString:responseObject]);
                                              });
                                              NSLog(@"saveUser: Received: %@", responseObject);
                                              NSLog(@"saveUser: Received HTTP %d", (int)httpResponse.statusCode);
                                          }
                                          
                                      }
                                        failure:^(NSURLSessionDataTask *task, NSError *error) {
                                          [SVProgressHUD showErrorWithStatus: error.localizedDescription];
                                          NSLog(@"saveUser: error: %@", error);
                                          completion(nil, error);
                                      }];
        
        
        return task;
    }


#pragma mark - recipes

- (NSURLSessionDataTask *)saveRecipe :(NSDictionary *) params completion:( void (^)(Recipe *recipe, NSError *error) )completion {
    [SVProgressHUD show];
    NSString *recipeId = [params objectForKey:@"_id"];
    NSString *pathUrl;
    if([recipeId isEqualToString:@""] || !recipeId) {
        pathUrl = @"/recipes";
    }
    else {
        pathUrl = [NSString stringWithFormat:@"/recipes/%@", recipeId];
    }
    NSLog(@"pamars: %@", params);
    NSLog(@"path: %@", pathUrl);
    
    NSURLSessionDataTask *task = [self POST:pathUrl
                                 parameters:params
                  constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                      NSData *image = [params objectForKey:@"photo"];
                      if(image) {
                          NSDate *date = [NSDate date];
                          NSString *filename = [NSString stringWithFormat:@"u_%@_%lf_%@.png", [params objectForKey:@"user"], [date timeIntervalSince1970], [SCHelp generateRandomString:5]];
                          [formData appendPartWithFileData: [params objectForKey:@"photo"]
                                                      name:@"image"
                                                  fileName:filename
                                                  mimeType:@"image/png"];
                      }
                      
                  } success:^(NSURLSessionDataTask *task, id responseObject) {
                      NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
                      if (httpResponse.statusCode == 200) {
                          NSLog(@"Repsonse: %@", responseObject);
                          Recipe *recipe = [[Recipe alloc] initWithProperties:responseObject];
                          
                          dispatch_async(dispatch_get_main_queue(), ^{
                              [SVProgressHUD dismiss];
                              completion(recipe, nil);
                          });
                      } else {
                          dispatch_async(dispatch_get_main_queue(), ^{
                              [SVProgressHUD showErrorWithStatus:@":("];
                              completion(nil, nil);
                          });
                          NSLog(@"Received: %@", responseObject);
                          NSLog(@"Received HTTP %d", (int)httpResponse.statusCode);
                      }
                      
                  } failure:^(NSURLSessionDataTask *task, NSError *error) {
                      [SVProgressHUD showErrorWithStatus:@":("];
                      NSLog(@"error: %@", error);
                  }];
    
    
    return task;
}

-(NSArray *) recipesForResponse: (NSDictionary *) response {
    NSMutableArray *recipes = [[NSMutableArray alloc] init];
    for (NSDictionary *recipe in response) {
        [recipes addObject: [Recipe objectWithProperties: recipe]];
    }
    return recipes;
}


- (NSURLSessionDataTask *)recipes :(NSDictionary *) params completion:( void (^)(NSArray *results, NSError *error) )completion {
    [SVProgressHUD show];
    NSURLSessionDataTask *task = [self GET:@"/recipes"
                                parameters: params
                                   success:^(NSURLSessionDataTask *task, id responseObject) {
                                       NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
                                       if (httpResponse.statusCode == 200) {
                                           NSArray *recipes = [self recipesForResponse:responseObject];
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               [SVProgressHUD dismiss];
                                               completion(recipes, nil);
                                           });
                                       } else {
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               [SVProgressHUD showErrorWithStatus:@":("];
                                               completion(nil, nil);
                                           });
                                           NSLog(@"Received: %@", responseObject);
                                           NSLog(@"Received HTTP %d", (int)httpResponse.statusCode);
                                       }
                                       
                                   } failure:^(NSURLSessionDataTask *task, NSError *error) {
                                       [SVProgressHUD showErrorWithStatus:@":("];
                                       NSLog(@"error: %@", error);
                                       dispatch_async(dispatch_get_main_queue(), ^{
                                           completion(nil, error);
                                       });
                                   }];
    return task;
}


- (NSURLSessionDataTask *)recipesSearch :(NSDictionary *) params completion:( void (^)(NSArray *results, NSError *error) )completion {
    [SVProgressHUD show];
    NSURLSessionDataTask *task = [self GET:@"/recipes/search"
                                parameters: params
                                   success:^(NSURLSessionDataTask *task, id responseObject) {
                                       NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
                                       if (httpResponse.statusCode == 200) {
                                           //NSLog(@"Repsonse: %@", responseObject);
                                           NSArray *recipes = [self recipesForResponse:responseObject];
                                           
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               [SVProgressHUD dismiss];
                                               completion(recipes, nil);
                                           });
                                       } else {
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               [SVProgressHUD showErrorWithStatus:@":("];
                                               completion(nil, nil);
                                           });
                                           NSLog(@"Received: %@", responseObject);
                                           NSLog(@"Received HTTP %d", (int)httpResponse.statusCode);
                                       }
                                       
                                   } failure:^(NSURLSessionDataTask *task, NSError *error) {
                                       [SVProgressHUD showErrorWithStatus:@":("];
                                       NSLog(@"error: %@", error);
                                       //dispatch_async(dispatch_get_main_queue(), ^{
                                       //    completion(nil, error);
                                       // });
                                   }];
    
    return task;
}



    @end
