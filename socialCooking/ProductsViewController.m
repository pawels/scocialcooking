//
//  ProductsViewController.m
//  socialCooking
//
//  Created by Pawel Sobocinski on 10.01.2014.
//  Copyright (c) 2014 Pawel Sobocinski. All rights reserved.
//

#import "ProductsViewController.h"
#import "SCApiClient.h"
#import "Product.h"
#import "ProductCell.h"
#import "SCAccount.h"

@interface ProductsViewController ()
    @property (nonatomic, strong) NSMutableArray *results;
@end

@implementation ProductsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Products";
    NSDictionary *params;
    [self featchProducts: params];
    
    

    // Uncomment the following line to preserve selection between presentations.
     //self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) featchProducts: (NSDictionary *) params {
    NSLog(@"%@", params);
    
    NSURLSessionDataTask *task = [[SCApiClient sharedClient] products:params completion:^(NSArray *results, NSError *error) {
        if (results) {
            _results = results;
            [self.tableView reloadData];
            [self.refreshControl endRefreshing];
        } else {
            NSLog(@"ERROR: %@", error);
        }
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self featchProducts: @{@"page": @"0"}];
}

- (void)clearResults {
    self.results = nil;
    [self.tableView reloadData];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(_results) {
        return _results.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"productCell";
    ProductCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    Product *product =  self.results[indexPath.row];
    cell.productId = product.productId;
    cell.productNameLabel.text = product.name;
    cell.usernameLabel.text = product.user.username;
    cell.userId = product.user.userId;
    NSLog(@"product user: %@", product.user.userId);
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!_selectedRows) {
        _selectedRows = [[NSMutableArray alloc] init];
    }
    ProductCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    //Maybe some validation here (such as duplicates etc)
    Product *product =  self.results[indexPath.row];
    //if exists remove
    if ([_selectedRows containsObject:product.productId]) {
        [_selectedRows removeObject:product.productId];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    //add to selected
    else {
        [_selectedRows addObject:product.productId];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    
  
    
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    ProductCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    User *user = [[SCAccount instance] user];
    if([cell.userId isEqualToString: user.userId]) {
        return YES;
    }
    return NO;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        Product *product = [_results objectAtIndex: indexPath.row];
        [[SCApiClient sharedClient] removeProduct:product.productId
                                       completion:^(BOOL *success, NSError *error) {
                                           if(success) {
                                               
                                               [_results removeObject:product];
                                               
                                               [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
                                           }
                                    
                                       }];
        
        
        
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
    
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

#pragma mark - UISearchBarDelegate

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    return YES;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    [self featchProducts: @{@"query": searchBar.text}];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar {
    [searchBar resignFirstResponder];
    [self featchProducts: @{@"page": @"0"}];
}



- (IBAction)refreshData:(id)sender {
    [self featchProducts: @{@"page": @"0"}];
}
@end
