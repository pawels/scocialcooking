//
//  StartViewController.m
//  socialCooking
//
//  Created by Pawel Sobocinski on 13.01.2014.
//  Copyright (c) 2014 Pawel Sobocinski. All rights reserved.
//

#import "StartViewController.h"

@interface StartViewController ()

@end

@implementation StartViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.hidesBottomBarWhenPushed = YES;
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onAccountStoreChanged:)
                                                 name:ACAccountStoreDidChangeNotification
                                               object:nil];
	// Do any additional setup after loading the view.
}

- (void)onAccountStoreChanged:(NSNotification *)notification {
    if ([self presentedViewController]) {
        [self dismissViewControllerAnimated:YES completion:^{
            [[SCAccount instance] setupAccountStore];
        }];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)facebookLogin:(id)sender {
    NSMutableArray *tabbarViewControllers = [NSMutableArray arrayWithArray: [self.tabBarController viewControllers]];
    NSLog(@"arr: %@", tabbarViewControllers);
    
    [[SCAccount instance] facebookLogin:^(BOOL logged) {
        if(logged) {
            NSMutableArray * vcs = [NSMutableArray
                                    arrayWithArray:[self.tabBarController viewControllers]];
            [vcs removeObjectAtIndex:0];
            [self.tabBarController setViewControllers:vcs];
        }
    }];
}
@end
