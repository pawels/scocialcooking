//
//  ProductViewController.h
//  socialCooking
//
//  Created by Pawel Sobocinski on 12.01.2014.
//  Copyright (c) 2014 Pawel Sobocinski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductViewController : UITableViewController
@property (strong, nonatomic) IBOutlet UITextField *nameField;
- (IBAction)saveProduct:(id)sender;

@end
