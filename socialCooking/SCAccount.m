//
//  UserGlobal.m
//  socialCooking
//
//  Created by Pawel Sobocinski on 13.01.2014.
//  Copyright (c) 2014 Pawel Sobocinski. All rights reserved.
//

#import "SCAccount.h"
#import <SVProgressHUD.h>
#import "MainTabViewController.h"


#define FACEBOOK_APP_ID  @"644474212260615"
#define FACEBOOK_APP_SCOPE  @[@"email"]

@implementation SCAccount

+(SCAccount *) instance {
    static SCAccount *_instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[SCAccount alloc] init];
    });
    return _instance;
}

#pragma mark - account

-(ACAccountStore* )accountStore {
    if(!_accountStore) {
        _accountStore = [[ACAccountStore alloc] init];
    }
    return _accountStore;
}


-(ACAccountStore* )setupAccountStore {
    _accountStore = [[ACAccountStore alloc] init];
    return [self accountStore];
}

#pragma mark - facebook
-(void) facebookLogin: ( void (^)(BOOL logged))completion {
    [SVProgressHUD show];
    NSLog(@"users login facebook");
    ACAccountType *facebookAccountType = [self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
    id options = @{
                   ACFacebookAppIdKey: FACEBOOK_APP_ID,
                   ACFacebookPermissionsKey: FACEBOOK_APP_SCOPE,
                   ACFacebookAudienceKey: ACFacebookAudienceFriends
                   };
    
    
    [self.accountStore requestAccessToAccountsWithType:facebookAccountType
                                               options:options
                                            completion:^(BOOL granted, NSError *error) {
                                                if (granted) {
                                                     NSLog(@"Granted!");
                                                    ACAccount *fbAccount = [[self.accountStore accountsWithAccountType:facebookAccountType] lastObject];
                                                    //load from preferences
                                                    
                                                    
                                                    //or get user info from facebook
                                                    [self meFacebookGraph:fbAccount completion: completion];
                                                    
                                                    
//                                                    //search for user in db by email
//                                                    NSString *userEmail = [fbAccount valueForKeyPath:@"properties.ACUIDisplayUsername"];
//                                                    NSLog(@"email %@", userEmail);
//                                                    [self searchUserByEmail:userEmail fbAccount:fbAccount];
//                                                    
//                                                    
//
//                                                    
//                                                    NSLog(@"%@", [fbAccount username]);
//                                                    NSLog(@"%@", fbAccount);
//                                                    NSLog(@"facebook account =%@",[fbAccount valueForKeyPath:@"properties.uid"]);
//                                                    NSLog(@"facebook account =%@",[fbAccount valueForKeyPath:@"properties.ACUIDisplayUsername"]);
//                                                    NSLog(@"%@", [fbAccount userFullName]);
//                                                    NSLog(@"%@", [fbAccount identifier]);
//                                                    NSLog(@"%@", [fbAccount accountDescription]);
                                                    
                                                    //[self fetchFacebookFriendsFor:fbAccount];
                                                } else {
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        // Fail gracefully...
                                                        NSLog(@"%@",error.description);
                                                        if([error code]== ACErrorAccountNotFound) {
                                                            [SVProgressHUD showErrorWithStatus:@"Account not found. Please setup your account in settings app."];

                                                            NSLog(@"@account not found");
                                                        }
                                                        else {
                                                            [SVProgressHUD showErrorWithStatus:@"Account access denied."];
                                                            NSLog(@"Access denied.");
                                                        }
                                                        
                                                        
                                                    });
                                                }
                                            }];
}

-(void) meFacebookGraph: (ACAccount *) fbAccount completion: ( void (^)(BOOL loegged))completion {
    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeFacebook
                                            requestMethod:SLRequestMethodGET
                                                      URL:[NSURL URLWithString:@"https://graph.facebook.com/me"]
                                               parameters:nil];
    request.account = fbAccount; // This is the _account from your code
    [request performRequestWithHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSHTTPURLResponse *responseHttp = ((NSHTTPURLResponse *)response);
        if (error == nil && responseHttp.statusCode == 200) {
            NSError *deserializationError;
            NSDictionary *userData = [NSJSONSerialization JSONObjectWithData:data options:0 error:&deserializationError];
            
            if (userData != nil && deserializationError == nil) {
                //search user by email
                [self searchUser: userData fbAccount:fbAccount completion: completion];
            }
            else {
                 dispatch_async(dispatch_get_main_queue(), ^{
                 [SVProgressHUD showErrorWithStatus: @"Facebook response error, try again."];
                });
            }
        }
        else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD showErrorWithStatus: [error localizedDescription]];
                NSLog(@"meFacebookGraph: %@", error);
                NSLog(@"response: %d", responseHttp.statusCode);
            });
            
            
            
            
        }
    }];
}

#pragma mark - users
/*
 * Search user o create account
 */
-(void) searchUser: (NSDictionary *) params fbAccount: (ACAccount *) fbAccount completion: ( void (^)(BOOL logged))completion   {
    NSString *userEmail = [params objectForKey:@"email"];
    [[SCApiClient sharedClient] userByEmail:userEmail completion:^(BOOL found, User *user, NSError *error) {
        //blad
        if(error) {
            NSLog(@"Error in \"searchUserByEmail\": %@", error);
            [SVProgressHUD showErrorWithStatus:@":("];
            completion(NO);
        }
        //znaleziono uzytkonika
        else if (found && user) {
            completion(YES);
            _user = user;
            //NSLog(@"user account %@", user);
            

        }
        //nie ma, trzeba zalozyc konto
        else {
            //NSLog(@"creating account");
            [[SCApiClient sharedClient] saveFacebookUser:params completion:^(User *user, NSError *error) {
                //uzytkonik stworzony
                //@todo zapisac do userbackground default
                if(!error && user) {
                    _user = user;
                    completion(YES);
                    
                }
                //nie udalo sie stworzyc
                else {
                    completion(NO);
                    NSLog(@"searchUser: userByEmail: saveFacebookUser: error: %@", error);
                }
            }];
            
            
            
        }
    }];
}


@end
