//
//  FindRequestController.m
//  socialCooking
//
//  Created by Pawel Sobocinski on 22.01.2014.
//  Copyright (c) 2014 Pawel Sobocinski. All rights reserved.
//

#import "FindRequestController.h"
#import "SCApiClient.h"
#import "SCHelp.h"
#import "RequestCell.h"
#import "Application.h"
#import <UIImageView+AFNetworking.h>
#import "RecipiesSearchViewController.h"

@interface FindRequestController ()
     @property (nonatomic, strong) NSArray *results;
@end

@implementation FindRequestController

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"products %@", _products);
    [self featchApplications];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void) featchApplications {
    [[SCApiClient sharedClient] appgeo:@{
                                         @"longitude": [NSString stringWithFormat:@"%lf", _currentLocation.coordinate.longitude],
                                         @"latitude": [NSString stringWithFormat:@"%lf", _currentLocation.coordinate.latitude]
                                         }
                            completion:^(NSArray *results, NSError *error) {
                                if (results) {
                                    _results = results;
                                    [self.tableView reloadData];
                                    [self.refreshControl endRefreshing];
                                } else {
                                    NSLog(@"ERROR: %@", error);
                                }
                            }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _results.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 110.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"RequestCell";
    RequestCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    Application *application =  self.results[indexPath.row];
    cell.applicationId = application.applicationId;
    UIImage *applicationImagePlaceholder = [UIImage imageNamed:@"default_avatar.png"];
    [cell.applicationImage setImageWithURL: [SCHelp fbAvatarUrl: application.user.fbUserId] placeholderImage:applicationImagePlaceholder];
    cell.applicationDistance.text = [SCHelp textDistance: [SCHelp distanceFrom: _currentLocation and: application.loc]];
    cell.applicationUser.text = application.user.username;
    cell.applicationCreated.text = [SCHelp dateForamted: application.created];
    cell.applicationCity.text = application.locationName;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView  willDisplayCell:(RequestCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *postname = [NSString stringWithFormat:@"labelUpdate_%@", cell.applicationId];
    [[NSNotificationCenter defaultCenter] addObserver: cell selector:@selector(updateLabel:) name:postname object: nil];
}

- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(RequestCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *postname = [NSString stringWithFormat:@"labelUpdate_%@", cell.applicationId];\
    [[NSNotificationCenter defaultCenter] removeObserver:cell name:postname object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    for (RequestCell *cell in self.tableView.visibleCells) {
        NSString *postname = [NSString stringWithFormat:@"labelUpdate_%@", cell.applicationId];
        [[NSNotificationCenter defaultCenter] removeObserver:cell name:postname object:nil];
    }
}


#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"recipes"]) {
        RecipiesSearchViewController *destViewController = segue.destinationViewController;
        NSIndexPath *ip = [self.tableView indexPathForSelectedRow];

        NSMutableArray *products = [NSMutableArray arrayWithArray:_products];
        Application *app = [_results objectAtIndex:ip.row];
        [products addObjectsFromArray: app.products];
        for (NSString *productId in app.products) {
            if(![products containsObject: productId]) {
                [products addObject: productId];
            }
        }
       
        destViewController.products = products;

    }

}

 

@end
