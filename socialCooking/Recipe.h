//
//  Recipe.h
//  sCookingRestKit_v2
//
//  Created by Pawel Sobocinski on 30.12.2013.
//  Copyright (c) 2013 Pawel Sobocinski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SCModel.h"
#import "User.h"

@interface Recipe : SCModel

@property (nonatomic, strong)  NSString *recipeId;
@property (nonatomic, strong)  NSString *name;
@property (nonatomic, strong)  NSArray *products;
@property (nonatomic, strong)  NSDate *created;
@property (nonatomic, strong) User *user;
@property (nonatomic, strong) NSData *photo;


-(void) saveRecipe:( void (^)(Recipe *recipe, NSError *error) )completion;

@end
