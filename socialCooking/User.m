//
//  User.m
//  sCookingRestKit_v2
//
//  Created by Pawel Sobocinski on 30.12.2013.
//  Copyright (c) 2013 Pawel Sobocinski. All rights reserved.
//

#import "User.h"

@implementation User

-(void)setValue:(id)value forKey:(NSString *)key
{
    if ([key isEqualToString:@"_id"]) {
        self.userId = value;
    }
    else if ([key isEqualToString:@"username"]) {
        self.username = value;
    }
    else if ([key isEqualToString:@"email"]) {
        self.email = value;
    }
    else if ([key isEqualToString:@"facebook"]) {
        self.fbUserId = [value objectForKey:@"id"];
    }
    else if ([key isEqualToString:@"created"]) {
        self.created = [self.dateFormater dateFromString:value];
    }
}

+(void)aricheObject:(User *)object key:(NSString *)key {
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:object];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:encodedObject forKey:key];
    [defaults synchronize];
    
}

+(User *)unArchiveObject:(NSString *)key {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *encodedObject = [defaults objectForKey:key];
    User *object = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    return object;
}



- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.userId forKey:@"userId"];
    [encoder encodeObject:self.username forKey:@"username"];
    [encoder encodeObject:self.fbUserId forKey:@"fbUserId"];
    [encoder encodeObject:self.created forKey:@"created"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        self.userId = [decoder decodeObjectForKey:@"userId"];
        self.username = [decoder decodeObjectForKey:@"username"];
        self.fbUserId = [decoder decodeObjectForKey:@"fbUserId"];
        self.created = [decoder decodeObjectForKey:@"created"];
    }
    NSLog(@"endoce %@", self.userId);
    return self;
}

@end
