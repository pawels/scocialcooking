//
//  UserGlobal.h
//  socialCooking
//
//  Created by Pawel Sobocinski on 13.01.2014.
//  Copyright (c) 2014 Pawel Sobocinski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
#import "SCHelp.h"
#import <Social/Social.h>
#import <Accounts/Accounts.h>
#import "SCApiClient.h"


@interface SCAccount : NSObject

@property (nonatomic, strong) ACAccountStore *accountStore;
@property (nonatomic, strong) User *user;

+ (SCAccount *) instance;


//account store
-(ACAccountStore* )setupAccountStore;
-(ACAccountStore* )accountStore;

-(void) facebookLogin: (void (^)(BOOL logged))completion;

@end
