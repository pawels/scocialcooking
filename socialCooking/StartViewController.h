//
//  StartViewController.h
//  socialCooking
//
//  Created by Pawel Sobocinski on 13.01.2014.
//  Copyright (c) 2014 Pawel Sobocinski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCAccount.h"


@interface StartViewController : UIViewController

- (IBAction)facebookLogin:(id)sender;

@end
