//
//  RequestNewViewController.h
//  socialCooking
//
//  Created by Pawel Sobocinski on 15.01.2014.
//  Copyright (c) 2014 Pawel Sobocinski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h> 

@interface RequestNewViewController : UITableViewController <CLLocationManagerDelegate>
@property (strong, nonatomic) IBOutlet UILabel *requestLocationLabel;
@property (strong, nonatomic) IBOutlet UIView *productsView;

@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) CLLocation *currentLocation;

- (IBAction)saveRequest:(id)sender;
@end
