//
//  ProductViewController.m
//  socialCooking
//
//  Created by Pawel Sobocinski on 12.01.2014.
//  Copyright (c) 2014 Pawel Sobocinski. All rights reserved.
//

#import "ProductViewController.h"
#import "Product.h"
#import "SCAccount.h"
#import "SCApiClient.h"


@interface ProductViewController ()

@end

@implementation ProductViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)saveProduct:(id)sender {
    [self.view endEditing:YES];
    Product *product = [[Product alloc] initWithProperties:@{
                                                       @"name": _nameField.text
                                                       }];
    product.user = [[SCAccount instance] user];
    [product saveProduct:^(Product *product, NSError *error) {
        if (!error) {
            [self.navigationController popViewControllerAnimated:YES];
            NSLog(@"saved product %@", product);
            //cofawy kontroller
            
        } else {
            NSLog(@"ERROR: %@", error);
        }
        }];
}


@end
