//
//  RequestViewController.h
//  socialCooking
//
//  Created by Paweł Sobociński on 08.01.2014.
//  Copyright (c) 2014 Pawel Sobocinski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Application.h"
#import <MapKit/MapKit.h>

@interface RequestViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, MKMapViewDelegate>

//view
@property (strong, nonatomic) IBOutlet UIScrollView *contentScrollView;
@property (strong, nonatomic) IBOutlet UIImageView *applicationImage;
@property (strong, nonatomic) IBOutlet UILabel *distanceToUserLabel;
@property (strong, nonatomic) IBOutlet UILabel *usernameLabel;
@property (strong, nonatomic) IBOutlet UILabel *citynameLabel;
@property (strong, nonatomic) IBOutlet UILabel *createdLabel;
@property (strong, nonatomic) IBOutlet UITableView *productsTable;

@property (strong, nonatomic) IBOutlet MKMapView *applicationMap;


//code
@property (nonatomic, strong) Application * application;
@property (nonatomic, strong) CLLocation *currentLocation;


@end
