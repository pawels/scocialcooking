//
//  RecipiesSearchViewController.h
//  socialCooking
//
//  Created by Pawel Sobocinski on 24.01.2014.
//  Copyright (c) 2014 Pawel Sobocinski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecipiesSearchViewController : UITableViewController
    @property (nonatomic, strong) NSMutableArray * products;
@end
