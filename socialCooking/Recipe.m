//
//  Recipe.m
//  sCookingRestKit_v2
//
//  Created by Pawel Sobocinski on 30.12.2013.
//  Copyright (c) 2013 Pawel Sobocinski. All rights reserved.
//

#import "Recipe.h"
#import "SCApiClient.h"

@implementation Recipe


-(void) saveRecipe:( void (^)(Recipe *recipe, NSError *error) )completion {
    NSDictionary *params = @{
                             @"user": self.user.userId,
                             @"name": self.name,
                             @"products": self.products,
                             @"photo" : self.photo
                             };
    

    [[SCApiClient sharedClient] saveRecipe:params completion:completion];
}

-(void)setValue:(id)value forKey:(NSString *)key
{
    if ([key isEqualToString:@"_id"]) {
        self.recipeId = value;
    }
    else if ([key isEqualToString:@"name"]) {
        self.name = value;
    }
    else if ([key isEqualToString:@"products"]) {
        self.products = value;
    }
    else if ([key isEqualToString:@"user"]) {
        self.user = [User objectWithProperties: value];
    }
    else if ([key isEqualToString:@"created"]) {
        self.created = [self.dateFormater dateFromString:value];
    }
}

@end
