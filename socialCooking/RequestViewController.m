//
//  RequestViewController.m
//  socialCooking
//
//  Created by Paweł Sobociński on 08.01.2014.
//  Copyright (c) 2014 Pawel Sobocinski. All rights reserved.
//

#import "RequestViewController.h"
#import "Application.h"
#import <UIImageView+AFNetworking.h>
#import "SCHelp.h"
#import "Product.h"
#import "ProductCell.h"
#import "SCApiClient.h"

@interface RequestViewController ()
    
@end

@implementation RequestViewController




- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if(_application) {
        _application.products = [[NSArray alloc] init];
        [[SCApiClient sharedClient] applications:@{@"_id": _application.applicationId} completion:^(NSArray *results, NSError *error) {
            if (!error) {
                Application *app = [results firstObject];
                
                 _application.products = app.products;
                [self fetchApplicationView];
                [_productsTable reloadData];
            }
           
        }];
    }
   
}

-(void) fetchApplicationView {
    NSLog(@"%@", _application.user.fbUserId);
    //app image
    UIImage *applicationImagePlaceholder = [UIImage imageNamed:@"default_avatar.png"];
    [_applicationImage setImageWithURL: [SCHelp fbAvatarUrl: _application.user.fbUserId] placeholderImage:applicationImagePlaceholder];
    
    
    _usernameLabel.text = _application.user.username;
    _createdLabel.text = [SCHelp dateForamted: _application.created];
    _citynameLabel.text = _application.locationName;
    _distanceToUserLabel.text = [SCHelp textDistance: [SCHelp distanceFrom: _currentLocation and: _application.loc]];
    
    
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    [annotation setCoordinate: _application.loc];
    [annotation setTitle:@"Title"];
    
    [_applicationMap addAnnotation:annotation];
    [_applicationMap setCenterCoordinate:_application.loc];
     MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance (_application.loc, 550, 550);
    [_applicationMap setRegion:region animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - table products

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(_application.products.count) {
        return _application.products.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
 
    static NSString *CellIdentifier = @"productCell";
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    Product *product =  _application.products[indexPath.row];
    //cell.productId = product.productId;
    cell.textLabel.text = product.name;
    cell.detailTextLabel.text = product.user.username;
    //cell.productNameLabel.text = product.name;
    //cell.usernameLabel.text = product.user.username;
    //cell.userId = @"0";
    
    NSLog(@"PPName: %@", product.user.username);

    return cell;
}


@end
