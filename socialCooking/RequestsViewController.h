//
//  RequestsViewController.h
//  cookingApp
//
//  Created by Pawel Sobocinski on 07.01.2014.
//  Copyright (c) 2014 Pawel Sobocinski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h> 

@interface RequestsViewController : UITableViewController <CLLocationManagerDelegate>
- (IBAction)refreshData:(id)sender;

@end
