//
//  SCHelp.m
//  socialCooking
//
//  Created by Pawel Sobocinski on 09.01.2014.
//  Copyright (c) 2014 Pawel Sobocinski. All rights reserved.
//

#import "SCHelp.h"

//#define API_BASE_URL @"http://localhost:3000/"
#define API_BASE_URL @"http://social-cooking.herokuapp.com/"


@implementation SCHelp


+(NSURL *) fbAvatarUrl: (NSString *) userId {
    return [NSURL URLWithString: [SCHelp fbAvatarString:userId]];
}
+(NSString *) fbAvatarString: (NSString *) userId {
    return [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?width=100&height=100", userId];
}

+(NSURL *) recipePictureUrl: (NSString *) pictureId {
    return [NSURL URLWithString: [SCHelp recipePictureString:pictureId]];
}

+(NSString *) recipePictureString: (NSString *) pictureId {
    return [NSString stringWithFormat:@"%@recipes/%@/picture", API_BASE_URL, pictureId];
}

+(NSString *) dateForamted: (NSDate *) date {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd MMM, HH:MM"];
    return [dateFormatter stringFromDate: date];
}

+(double) distanceBetween: (CLLocationCoordinate2D) firstLocation and: (CLLocationCoordinate2D) secondLocation {
    
    CLLocation *location = [[CLLocation alloc] initWithLatitude:firstLocation.latitude longitude:firstLocation.longitude];
    
    return [SCHelp distanceFrom:location and:secondLocation];
}

+(double) distanceFrom: (CLLocation *) location and: (CLLocationCoordinate2D ) cordinates {
    CLLocation *second = [[CLLocation alloc] initWithLatitude: cordinates.latitude longitude: cordinates.longitude];
    CLLocationDistance dist = [location distanceFromLocation:second];
    return dist;
}

+(NSString *) textDistance: (double) distance {
    if (distance >= 1000.0) {
        distance = distance/1000.0;
        return [NSString stringWithFormat:@"%.2f km", distance];
    }
    return [NSString stringWithFormat:@"%.2f m", distance];
}


+(void) getCityNameFrom: (CLLocation *)location completion:(SCLocationName)completion {
    CLGeocoder *reverseGeocoder = [[CLGeocoder alloc] init];
    
    [reverseGeocoder reverseGeocodeLocation: location completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if (error){
             completion(nil, error);
             NSLog(@"Geocode failed with error: %@", error);
             return;
         }
         
        CLPlacemark *myPlacemark = [placemarks firstObject];
         //NSString *countryCode = myPlacemark.ISOcountryCode;
         NSString *countryName = myPlacemark.country;
         NSString *city = myPlacemark.addressDictionary[@"City"];
         
         NSString *name = [NSString stringWithFormat:@"%@, %@", city, countryName];
         completion(name, nil);
     }];
}

+(NSError *) errorFromString: (NSString *) message {
    NSString *domain = @"agudo.socialCooking";
    NSString *desc = message;
    NSDictionary *userInfo = [[NSDictionary alloc]
                              initWithObjectsAndKeys:desc,
                              @"NSLocalizedDescriptionKey",NULL];
    NSError *errorPtr = [NSError errorWithDomain:domain code:-101
                                userInfo:userInfo];
    return errorPtr;
}


+(NSString*)generateRandomString:(int)num {
    NSMutableString* string = [NSMutableString stringWithCapacity:num];
    for (int i = 0; i < num; i++) {
        [string appendFormat:@"%C", (unichar)('a' + arc4random_uniform(25))];
    }
    return string;
}

@end
