//
//  FindRequestController.h
//  socialCooking
//
//  Created by Pawel Sobocinski on 22.01.2014.
//  Copyright (c) 2014 Pawel Sobocinski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface FindRequestController : UITableViewController
    @property (nonatomic, strong) CLLocationManager *locationManager;
    @property (nonatomic, strong) CLLocation *currentLocation;
    @property (nonatomic, strong) NSArray *products;
@end
