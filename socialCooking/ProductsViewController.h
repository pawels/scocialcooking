//
//  ProductsViewController.h
//  socialCooking
//
//  Created by Pawel Sobocinski on 10.01.2014.
//  Copyright (c) 2014 Pawel Sobocinski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductsViewController : UITableViewController <UISearchBarDelegate>
    @property (nonatomic, strong) NSMutableArray *selectedRows;
- (IBAction)refreshData:(id)sender;
@end
