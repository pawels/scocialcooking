//
//  RecipesViewController.m
//  socialCooking
//
//  Created by Pawel Sobocinski on 14.01.2014.
//  Copyright (c) 2014 Pawel Sobocinski. All rights reserved.
//

#import "RecipesViewController.h"
#import "SCApiClient.h"
#import <UIImageView+AFNetworking.h>
#import "SCHelp.h"

@interface RecipesViewController ()
    @property (nonatomic, strong) NSArray *results;
@end

@implementation RecipesViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self featchRecipes: @{}];
}

-(void) featchRecipes: (NSDictionary *) params {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    [[SCApiClient sharedClient] recipes:params completion:^(NSArray *results, NSError *error) {
        if (results) {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
            _results = results;
            [self.tableView reloadData];
            [self.refreshControl endRefreshing];
        } else {
            NSLog(@"featchRecipes: ERROR: %@", error);
        }
    }];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _results.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"recipeCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    Recipe *recipe =  self.results[indexPath.row];
    cell.textLabel.text = recipe.name;
    cell.detailTextLabel.text = recipe.user.username;
    UIImage *applicationImagePlaceholder = [UIImage imageNamed:@"default_avatar.png"];
    cell.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [cell.imageView setImageWithURL: [SCHelp recipePictureUrl: recipe.recipeId] placeholderImage:applicationImagePlaceholder];
    return cell;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self featchRecipes: @{@"page": @"0"}];
}

#pragma mark - UISearchBarDelegate
- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    return YES;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    //NSLog(@"serarch: %@", searchBar.text);
    [self featchRecipes: @{@"query": searchBar.text}];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar {
    [searchBar resignFirstResponder];
    [self featchRecipes: @{@"page": @"0"}];
}

- (IBAction)refreshData:(id)sender {
    [self featchRecipes: @{@"page": @"0"}];
}

@end
