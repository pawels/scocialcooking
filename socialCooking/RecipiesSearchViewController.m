//
//  RecipiesSearchViewController.m
//  socialCooking
//
//  Created by Pawel Sobocinski on 24.01.2014.
//  Copyright (c) 2014 Pawel Sobocinski. All rights reserved.
//

#import "RecipiesSearchViewController.h"
#import "SCApiClient.h"
#import "RecipeSearchCell.h"
#import "Recipe.h"
#import <UIImageView+AFNetworking.h>
#import "SCHelp.h"

@interface RecipiesSearchViewController ()
    @property (nonatomic, strong) NSArray* results;
@end

@implementation RecipiesSearchViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self fetchRecipes];
    
    NSLog(@"Products: %@", _products);
    

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) fetchRecipes {
    [[SCApiClient sharedClient] recipesSearch:@{@"products": _products} completion:^(NSArray *results, NSError *error) {
        if(results) {
            _results = results;
            [self.tableView reloadData];
        }
        else {
            NSLog(@"error on fetchRecipe: %@", error);
        }
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _results.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"recipe";
    RecipeSearchCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    Recipe *recipe = [_results objectAtIndex:indexPath.row];
    cell.recipe = recipe;
    cell.products = _products;
    cell.recipeTitle.text = recipe.name;
    
    UIImage *applicationImagePlaceholder = [UIImage imageNamed:@"default_avatar.png"];
    cell.recipeImage.contentMode = UIViewContentModeScaleAspectFit;
    [cell.recipeImage setImageWithURL: [SCHelp recipePictureUrl: recipe.recipeId] placeholderImage:applicationImagePlaceholder];
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

@end
