//
//  ProductCell.h
//  socialCooking
//
//  Created by Pawel Sobocinski on 10.01.2014.
//  Copyright (c) 2014 Pawel Sobocinski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *productNameLabel;

@property (strong, nonatomic) IBOutlet UILabel *usernameLabel;
@property (strong, nonatomic) NSString *productId;
@property (strong, nonatomic) NSString *userId;

@end
