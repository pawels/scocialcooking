//
//  SCModel.m
//  socialCooking
//
//  Created by Pawel Sobocinski on 08.01.2014.
//  Copyright (c) 2014 Pawel Sobocinski. All rights reserved.
//

#import "SCModel.h"

@implementation SCModel

@synthesize dateFormater = _dateFormater;


+(id)objectWithProperties:(NSDictionary *)properties
{
    return [[self alloc] initWithProperties:properties];
}

-(id)initWithProperties:(NSDictionary *)properties
{
    self = [super init];
    if (self) {
        [self setValuesForKeysWithDictionary:properties];
    }
    return self;
}

- (ISO8601DateFormatter*) dateFormater
{
    if (!_dateFormater){
        _dateFormater = [[ISO8601DateFormatter alloc] init];
    }
    return _dateFormater;
}

@end
