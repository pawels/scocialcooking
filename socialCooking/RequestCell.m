//
//  RequestCell.m
//  cookingApp
//
//  Created by Pawel Sobocinski on 07.01.2014.
//  Copyright (c) 2014 Pawel Sobocinski. All rights reserved.
//

#import "RequestCell.h"

@implementation RequestCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void) updateLabel:(NSNotification *)notification  {
    NSString * cityName = [notification object];
    //NSLog(@"update city: %@", cityName);
    self.applicationCity.text = cityName;
    self.applicationCity.hidden = NO;
}



@end
