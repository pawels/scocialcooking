//
//  SCModel.h
//  socialCooking
//
//  Created by Pawel Sobocinski on 08.01.2014.
//  Copyright (c) 2014 Pawel Sobocinski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ISO8601DateFormatter.h>
@interface SCModel : NSObject

@property (nonatomic, strong) ISO8601DateFormatter* dateFormater;

+(id)objectWithProperties:(NSDictionary *)properties;
-(id)initWithProperties:(NSDictionary *)properties;


@end
