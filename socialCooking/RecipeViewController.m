//
//  RecipeViewController.m
//  socialCooking
//
//  Created by Pawel Sobocinski on 12.01.2014.
//  Copyright (c) 2014 Pawel Sobocinski. All rights reserved.
//

#import "RecipeViewController.h"
#import "ProductsViewController.h"
#import "User.h"
#import "SCAccount.h"

@interface RecipeViewController ()
    @property (nonatomic, strong) ProductsViewController *productVC;
@end

@implementation RecipeViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    for(id childvc in self.childViewControllers) {
        if([childvc isKindOfClass:[ProductsViewController class]]) {
            _productVC = childvc;
        }
    }
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    
}

- (void)promptForPhoto {
    UIImagePickerController *pickerController = [[UIImagePickerController alloc] init];
    pickerController.sourceType =
    UIImagePickerControllerSourceTypePhotoLibrary |
    UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        pickerController.sourceType |=UIImagePickerControllerSourceTypeCamera;
    }
    
    pickerController.delegate = self;
    pickerController.allowsEditing = YES;
    
    [self presentViewController:pickerController
                       animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
    _recipePhoto.image = image;
    _recipePhoto.contentMode = UIViewContentModeScaleAspectFit;
    [self dismissViewControllerAnimated:YES completion:nil];
}



- (IBAction)saveRecipe:(id)sender {
    NSLog(@"%@", _productVC.selectedRows);
    
    
    [self.view endEditing:YES];
    Recipe *recipe = [[Recipe alloc] initWithProperties:@{
                                                             @"name": _recipeNameText.text
                                                             }];
    recipe.products = _productVC.selectedRows;
    recipe.user = [[SCAccount instance] user];
    if(self.recipePhoto.image) {
        recipe.photo = UIImagePNGRepresentation(self.recipePhoto.image);
    }
    
    [recipe saveRecipe:^(Recipe *recipe, NSError *error) {
        if (!error) {
            [self.navigationController popViewControllerAnimated:YES];
            NSLog(@"saved product %@", recipe);
            //cofawy kontroller
        } else {
            NSLog(@"ERROR: %@", error);
        }

    }];
    
    
    
}
- (IBAction)addPhoto:(id)sender {
    [self promptForPhoto];
    NSLog(@"selected: %@", _productVC.selectedRows);

}
@end
