//
//  SCHelp.h
//  socialCooking
//
//  Created by Pawel Sobocinski on 09.01.2014.
//  Copyright (c) 2014 Pawel Sobocinski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "SCApiClient.h"

typedef void (^SCLocationName)(NSString *locationName, NSError *error);

@interface SCHelp : NSObject

+(NSURL *) fbAvatarUrl: (NSString *) userId;
+(NSString *) fbAvatarString: (NSString *) userId;
+(NSURL *) recipePictureUrl: (NSString *) pictureId;
+(NSString *) recipePictureString: (NSString *) pictureId;


+(NSString *) dateForamted: (NSDate *) date;

+(double) distanceBetween: (CLLocationCoordinate2D) firstLocation and: (CLLocationCoordinate2D) secondLocation;

+(double) distanceFrom: (CLLocation *) location and: (CLLocationCoordinate2D) cordinates;

+(NSString *) textDistance: (double) distance;
+(void) getCityNameFrom: (CLLocation *)location completion:(SCLocationName)completion;
+(NSError *) errorFromString: (NSString *) message;


+(NSString*)generateRandomString:(int)num;
@end
