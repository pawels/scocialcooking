//
//  RequestCell.h
//  cookingApp
//
//  Created by Pawel Sobocinski on 07.01.2014.
//  Copyright (c) 2014 Pawel Sobocinski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RequestCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *applicationImage;
@property (strong, nonatomic) IBOutlet UILabel *applicationDistance;
@property (strong, nonatomic) IBOutlet UILabel *applicationUser;
@property (strong, nonatomic) IBOutlet UILabel *applicationCreated;
@property (strong, nonatomic) IBOutlet UILabel *applicationCity;

@property (strong, nonatomic) NSString *applicationId;

@end
