//
//  User.h
//  sCookingRestKit_v2
//
//  Created by Pawel Sobocinski on 30.12.2013.
//  Copyright (c) 2013 Pawel Sobocinski. All rights reserved.
//

#import "SCModel.h"
#import <Social/Social.h>
#import <Accounts/Accounts.h>


@interface User : SCModel

@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) NSString *fbUserId;
@property (nonatomic, copy) NSString *username;
@property (nonatomic, copy) NSString *firstname;
@property (nonatomic, copy) NSString *lastname;
@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSDate *created;


+(User *)unArchiveObject:(NSString *)key;
+(void)aricheObject:(User *)object key:(NSString *)key;



@end
