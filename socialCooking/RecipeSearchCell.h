//
//  RecipeSearchCell.h
//  socialCooking
//
//  Created by Pawel Sobocinski on 24.01.2014.
//  Copyright (c) 2014 Pawel Sobocinski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Recipe.h"

@interface RecipeSearchCell : UITableViewCell <UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UIImageView *recipeImage;
@property (strong, nonatomic) IBOutlet UILabel *recipeTitle;
@property (strong, nonatomic) IBOutlet UITableView *recipeProducts;


@property (strong, nonatomic) NSArray *products;
@property (strong, nonatomic) Recipe *recipe;

@end
