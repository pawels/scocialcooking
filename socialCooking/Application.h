//
//  Application.h
//  socialCooking
//
//  Created by Pawel Sobocinski on 08.01.2014.
//  Copyright (c) 2014 Pawel Sobocinski. All rights reserved.
//


#import "User.h"
#import <CoreLocation/CoreLocation.h>
#import "SCModel.h"
#import "RequestCell.h"

@interface Application : SCModel 

@property (nonatomic, strong) NSString *applicationId;
@property (nonatomic, strong) User *user;
@property (nonatomic, assign) CLLocationCoordinate2D loc;
@property (nonatomic, strong) NSString* locationName;
@property (nonatomic, strong) NSDate *created;
@property (nonatomic, strong) NSArray *products;


-(void) saveApplication:( void (^)(Application *application, NSError *error) )completion;

@end
