//
//  RequestNewViewController.m
//  socialCooking
//
//  Created by Pawel Sobocinski on 15.01.2014.
//  Copyright (c) 2014 Pawel Sobocinski. All rights reserved.
//

#import "RequestNewViewController.h"
#import "SCHelp.h"
#import "ProductsViewController.h"
#import "Application.h"
#import "SCAccount.h"
#import "FindRequestController.h"

@interface RequestNewViewController ()
    @property (nonatomic, strong) ProductsViewController *productVC;
@end

@implementation RequestNewViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //get location
    [self updateLocation];
    for(id childvc in self.childViewControllers) {
        if([childvc isKindOfClass:[ProductsViewController class]]) {
            _productVC = childvc;
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -locaion
- (void)updateLocation {
    [self.locationManager startUpdatingLocation];
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    _currentLocation = [locations lastObject];
    [SCHelp getCityNameFrom:_currentLocation completion:^(NSString *locationName, NSError *error) {
        if(!error) {
            _requestLocationLabel.text = locationName;
        }
        else {
            NSLog(@"locationManager: getCityNameFrom: error: %@", error);
        }

    }];
    
    [self.locationManager stopUpdatingLocation];
}

- (CLLocationManager *)locationManager {
    if (!_locationManager) {
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        _locationManager.delegate = self;
    }
    return _locationManager;
}

- (IBAction)saveRequest:(id)sender {
    NSLog(@"%@", _productVC.selectedRows);
    
    
    [self.view endEditing:YES];
    Application *application = [[Application alloc] initWithProperties:@{}];
    application.products = _productVC.selectedRows;
    application.user = [[SCAccount instance] user];
    application.loc = CLLocationCoordinate2DMake(_currentLocation.coordinate.latitude, _currentLocation.coordinate.longitude);
    
    
    
    [application saveApplication:^(Application *application, NSError *error) {
        if (!error) {
            [self.navigationController popViewControllerAnimated:YES];
            NSLog(@"saved request %@", application);
            //cofawy kontroller
        } else {
            NSLog(@"ERROR: %@", error);
        }
        
    }];
}


#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"search"]) {
        FindRequestController *destViewController = segue.destinationViewController;
        NSIndexPath *ip = [self.tableView indexPathForSelectedRow];
        
        //destViewController.application = [_results objectAtIndex:ip.row];
        destViewController.products = _productVC.selectedRows;
        destViewController.currentLocation = _currentLocation;
    }

}

@end
